package com.hipac.qihang;

import com.hipac.qihang.dao.BaseResponse;
import com.hipac.qihang.dao.SubTaskInfo;
import com.hipac.qihang.dao.SupplierShopHomeItem;
import com.hipac.qihang.dao.TaskInfo;
import com.hipac.qihang.dao.request.CategoryRequestParams;
import com.sun.jersey.spi.container.servlet.ServletContainer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @description 提供REST接口
 */
@Path("/")
public class RestInterface {


    /**
     * 根据类目id查询店铺分类下的商品.
     *
     * @param requestParams CategoryRequestParams
     * @return BaseResponse<List < SupplierShopHomeItem>>
     */
    @Path("/getCategoryGoods") // url上没有参数，参数通过body传入
    @POST
    @Consumes(MediaType.APPLICATION_JSON) // 声明传入参数是json格式
    @Produces(MediaType.APPLICATION_JSON)
    public BaseResponse<List<SupplierShopHomeItem>> getCategoryGoods(CategoryRequestParams requestParams) {
        List<SupplierShopHomeItem> dataList = generateGoodsList(requestParams.storeId, 15, requestParams.pageNo);
        BaseResponse response = new BaseResponse();
        response.api = "getCategoryGoods";
        response.data = dataList;
        response.totalCount = 100;
        response.totalPage = 100;
        response.pageNo = requestParams.pageNo;
        return response;
    }

    /**
     *
     * @param storeId 店铺id
     * @param sum 最大条数
     * @param pageNo 页码
     * @return 随机返回[0,sum]条数据
     */
    private List<SupplierShopHomeItem> generateGoodsList(String storeId, int sum, int pageNo) {
        List<SupplierShopHomeItem> result = new ArrayList<SupplierShopHomeItem>();
        int max = sum;
        int min = 0;
        Random random = new Random();
        int pageSum = random.nextInt(max) % (max - min + 1) + min;
        for (int i = 0; i < pageSum; i++) {
            SupplierShopHomeItem item = new SupplierShopHomeItem();
            item.setName("商品" + (pageNo * 100 + i));
            item.setStoreId(storeId);
            result.add(item);
        }
        return result;
    }


    /**
     * 测试用的main函数.
     */
    public static void main(String[] args) throws Exception {
        Server server = new Server(8080); // 监听8080端口，也可更改为其他端口
        ServletHolder servlet = new ServletHolder(ServletContainer.class);
        // 设置初始化参数
        servlet.setInitParameter("com.sun.jersey.config.property.resourceConfigClass", "com.sun.jersey.api.core.PackagesResourceConfig");
        servlet.setInitParameter("com.sun.jersey.config.property.packages", "com.hipac.qihang");
        servlet.setInitParameter("com.sun.jersey.api.json.POJOMappingFeature", "true"); // 自动将对象映射成json返回
        ServletContextHandler handler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        handler.setContextPath("/");
        handler.addServlet(servlet, "/*");
        server.setHandler(handler);
        server.start();
        System.out.println("start...in 8080");
    }
}
