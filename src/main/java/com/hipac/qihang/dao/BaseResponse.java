package com.hipac.qihang.dao;


import com.google.gson.JsonObject;

import java.io.Serializable;

public class BaseResponse<T> implements Serializable {
    public int code = 200;
    public String api;
    public String message = "";
    public boolean success = true;
    public int totalCount;
    public int totalPage;
    public int pageNo;
    public String v = "1.0.0";
    public T data;
    public BaseResponse.MonitorInfo monitorInfo = new MonitorInfo();

    public BaseResponse() {
    }

    public String toString() {
        return "BaseResponse{code=" + this.code + ", message='" + this.message + '\'' + ", success=" + this.success + ", totalCount=" + this.totalCount + ", totalPage=" + this.totalPage + ", pageNo=" + this.pageNo + ", data=" + this.data + '}';
    }

    public static class MonitorInfo implements Serializable {
        public String ppTraceId;
        public Long processTime = 685L;

        public MonitorInfo() {
        }
    }
}