package com.hipac.qihang.dao;


import java.io.Serializable;

public class SupplierShopHomeItem implements Serializable {


    /**
     * delivery : null
     * itemType : {"name":"国内贸易","value":"2"}
     * activeTag : 嗨清仓
     * itemTagCode : 17
     * dataType : item
     * itemPicMaskCode : 0
     * hotLevel : 3.5万
     * pic : http://img.hicdn.cn/item/201811/111514212749124Q2k.jpeg
     * isHiRateItem : false
     * cuePrice : {"name":null,"type":"originalPrice","value":"9.56-12.72"}
     * price : {"name":null,"type":"num","value":"9.20-12.20"}
     * extendFields : {"sort_type":3,"item_id":74823,"position":29,"model_id":900,"search_time":1546066987378,"search_text":""}
     * name : 【嗨清仓】限量5000把 8K情侣花色短柄三折伞折叠雨伞 款式随机
     * id : 74823
     * storeId : 8436690a6b8440ebbb6c10a4caaa3442
     */
    private NameValue itemType = new NameValue();
    private String activeTag = "嗨清仓";
    private int itemTagCode = 17;
    private String dataType;
    private int itemPicMaskCode;
    private String hotLevel = "3.5万";
    private String pic = "http://img.hicdn.cn/item/201711/11031735314696.jpeg";
    private boolean isHiRateItem;
    private TypeValue cuePrice = new TypeValue();
    private TypeValue price = new TypeValue();
    private String extendFields;
    private String name;
    private int id;
    private String storeId;
    private String supplierId;

    private transient int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isHiRateItem() {
        return isHiRateItem;
    }

    public void setHiRateItem(boolean hiRateItem) {
        isHiRateItem = hiRateItem;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public NameValue getItemType() {
        return itemType;
    }

    public void setItemType(NameValue itemType) {
        this.itemType = itemType;
    }

    public String getActiveTag() {
        return activeTag;
    }

    public void setActiveTag(String activeTag) {
        this.activeTag = activeTag;
    }

    public int getItemTagCode() {
        return itemTagCode;
    }

    public void setItemTagCode(int itemTagCode) {
        this.itemTagCode = itemTagCode;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public int getItemPicMaskCode() {
        return itemPicMaskCode;
    }

    public void setItemPicMaskCode(int itemPicMaskCode) {
        this.itemPicMaskCode = itemPicMaskCode;
    }

    public String getHotLevel() {
        return hotLevel;
    }

    public void setHotLevel(String hotLevel) {
        this.hotLevel = hotLevel;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public boolean isIsHiRateItem() {
        return isHiRateItem;
    }

    public void setIsHiRateItem(boolean isHiRateItem) {
        this.isHiRateItem = isHiRateItem;
    }

    public TypeValue getCuePrice() {
        return cuePrice;
    }

    public void setCuePrice(TypeValue cuePrice) {
        this.cuePrice = cuePrice;
    }

    public TypeValue getPrice() {
        return price;
    }

    public void setPrice(TypeValue price) {
        this.price = price;
    }

    public String getExtendFields() {
        return extendFields;
    }

    public void setExtendFields(String extendFields) {
        this.extendFields = extendFields;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }
}
