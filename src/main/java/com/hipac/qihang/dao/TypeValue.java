package com.hipac.qihang.dao;

import java.io.Serializable;

public class TypeValue implements Serializable {

    private String type = "num";

    private String value = "107.00";

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public TypeValue copy() {
        TypeValue typeValue = new TypeValue();
        typeValue.type = this.type;
        typeValue.value = this.value;
        return typeValue;
    }

    public static TypeValue newNum(String price) {
        TypeValue typeValue = new TypeValue();
        typeValue.type = "num";
        typeValue.value = price;
        return typeValue;
    }

}