package com.hipac.qihang.dao.request;

public class CategoryRequestParams {
    public int pageSize;
    public int pageNo;
    public String categoryId;
    public String storeId;
    public String sortType;
    public String sortOrder;
}
