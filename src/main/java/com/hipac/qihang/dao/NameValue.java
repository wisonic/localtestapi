package com.hipac.qihang.dao;

import java.io.Serializable;

public class NameValue implements Serializable {
    /**
     * name : 国内贸易
     * value : 2
     */

    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}